# learn-Rust-and-CICD

## Description
Udemy에서 공부한 GitLab CI/CD와 Rust 코드를 저장합니다.
- Store codes and `.gitlab-ci.yml` from Udemy lesson in my words.
## Badges
추후에 붙일 예정입니다.
- Added them later.

## Authors and acknowledgment
Thanks to
- [The Rust Programming Language](https://www.udemy.com/course/rust-lang/learn/lecture/4288542?start=15#overview)
- [GitLab CI - A Complete Hands-On for CI/CD Pipelines & DevOps](https://www.udemy.com/course/gitlab-cicd-course/learn/lecture/28109888?start=0#overview)

## License
MIT License

