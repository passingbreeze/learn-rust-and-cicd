terraform {
    required_version = ">= 1.0.0"

    required_providers {
      aws = {
        source = "hashicorp/aws"
        version = "~> 4.0"
      }
    }
    backend "http" {
      address = "https://gitlab.com/api/v4/projects/37236243/terraform/state/old-state-name"
      lock_address = "https://gitlab.com/api/v4/projects/37236243/terraform/state/old-state-name/lock"
      unlock_address = "https://gitlab.com/api/v4/projects/37236243/terraform/state/old-state-name/lock"
      username = "passingbreeze"
      password = "$GITLAB_PERSONAL_TOKEN"
      lock_method = "POST"
      unlock_method = "DELETE"
      retry_wait_min = 5
    }
}
